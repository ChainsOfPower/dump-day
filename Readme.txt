Demo1 folder sadrzi igru u flat js. Primjer je uzet sa W3Schools, a mozemo ga doradit tako da umjesto kvadrata stavimo sliku ili da ubacimo zvuk u igru,
ali mislim da je ovako dovoljno ako ćemo radit 2 demo-a.

Demo2 folder sadri pixie.js memory game. Tu ne treba nista mjenjat, primjer odlično prikazuje primjenu pixie.js i njegove prednosti u odnosu na sami js bez
library-a ili frameworka.

NOTE: Demo2 pokretati na localhost serveru!